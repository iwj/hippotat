#!/usr/bin/python3

from setuptools import setup, find_packages

import re as regexp
import glob
import sys

scripts = ['hippotat','hippotatd']
scan = scripts + glob.glob('hippotatlib/*.py')

def find_requires():
  mod_pat = r'[._0-9a-zA-Z]+'
  res = list(map(regexp.compile,
                 [r'from\s+('+mod_pat+r')\s+import\b',
                  r'import\s+('+mod_pat+r')\s']))
  reqs = { }
  for scanf in scan:
    print('scanning %s' % scanf, file=sys.stderr)
    for l in open(scanf):
      for re in res:
        m = re.match(l)
        if m is not None:
          reqs[m.group(1)] = True
          break
  print(repr(reqs), file=sys.stderr)
  return list(reqs.keys())

setup(
  name="hippotat",
  packages=find_packages(),
  install_requires=find_requires(),
  scripts=scripts
)
