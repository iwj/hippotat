#!/usr/bin/python3

from hippotatlib.ownsource import SourceShipmentPreparer

import twisted
import sys

p = SourceShipmentPreparer('tmp')
p.stream_debug = sys.stdout
p.generate()
