// Copyright 2021-2022 Ian Jackson and contributors to Hippotat
// SPDX-License-Identifier: GPL-3.0-or-later WITH LicenseRef-Hippotat-OpenSSL-Exception
// There is NO WARRANTY.

use crate::prelude::*;

#[derive(hippotat_macros::ResolveConfig)]
#[derive(Debug,Clone)]
pub struct InstanceConfig {
  // Exceptional settings
  #[special(special_link, SKL::None)]      pub    link:   LinkName,
  #[per_client]                            pub    secret: Secret,
  #[global] #[special(special_ipif, SKL::PerClient)] pub ipif: String,

  // Capped settings:
  #[limited] pub max_batch_down:               u32,
  #[limited] pub max_queue_time:               Duration,
  #[limited] pub http_timeout:                 Duration,
  #[limited] pub target_requests_outstanding:  u32,
  #[special(special_max_up, SKL::Limited)]  pub max_batch_up: u32,

  // Ordinary settings, used by both, not client-specifi:
  #[global]  pub addrs:                        Vec<IpAddr>,
  #[global]  pub vnetwork:                     Vec<IpNet>,
  #[global]  pub vaddr:                        IpAddr,
  #[global]  pub vrelay:                       IpAddr,
  #[global]  pub port:                         u16,
  #[global]  pub mtu:                          u32,

  // Ordinary settings, used by server only:
  #[server] #[per_client] pub max_clock_skew:               Duration,
  #[server] #[global]     pub ifname_server:                String,

  // Ordinary settings, used by client only:
  #[client]  pub http_timeout_grace:           Duration,
  #[client]  pub max_requests_outstanding:     u32,
  #[client]  pub http_retry:                   Duration,
  #[client]  pub success_report_interval:      Duration,
  #[client]  pub url:                          Url,
  #[client]  pub vroutes:                      Vec<IpNet>,
  #[client]  pub ifname_client:                String,

  // Computed, rather than looked up.  Client only:
  #[computed]  pub effective_http_timeout:     Duration,
}

static DEFAULT_CONFIG: &str = r#"
[COMMON]
max_batch_down = 65536
max_queue_time = 10
target_requests_outstanding = 3
http_timeout = 30
http_timeout_grace = 5
max_requests_outstanding = 6
max_batch_up = 4000
http_retry = 5
port = 80
vroutes = ''
ifname_client = hippo%d
ifname_server = shippo%d
max_clock_skew = 300
success_report_interval = 3600

ipif = userv root ipif %{local},%{peer},%{mtu},slip,%{ifname} '%{rnets}'

mtu = 1500

vnetwork = 172.24.230.192

[LIMIT]
max_batch_up = 262144
max_batch_down = 262144
max_queue_time = 121
http_timeout = 121
target_requests_outstanding = 10
"#;

#[derive(clap::Args,Debug)]
pub struct CommonOpts {
  /// Top-level config file or directory
  ///
  /// Look for `main.cfg`, `config.d` and `secrets.d` here.
  ///
  /// Or if this is a file, just read that file.
  #[clap(long, default_value="/etc/hippotat")]
  pub config: PathBuf,
  
  /// Additional config files or dirs, which can override the others
  #[clap(long, action=clap::ArgAction::Append)]
  pub extra_config: Vec<PathBuf>,
}

pub trait InspectableConfigAuto {
  fn inspect_key_auto(&self, field: &'_ str)
                      -> Option<&dyn InspectableConfigValue>;
}
pub trait InspectableConfig: Debug {
  fn inspect_key(&self, field: &'_ str)
                 -> Option<&dyn InspectableConfigValue>;
}

impl InspectableConfig for (&ServerName, &InstanceConfigGlobal) {
  fn inspect_key(&self, field: &'_ str)
                 -> Option<&dyn InspectableConfigValue> {
    Some(match field {
      "server" => self.0,
      k => return self.1.inspect_key_auto(k),
    })
  }
}

impl InspectableConfig for InstanceConfig {
  fn inspect_key(&self, field: &'_ str)
                 -> Option<&dyn InspectableConfigValue> {
    Some(match field {
      "link" => &self.link,
      "server" => &self.link.server,
      "client" => &self.link.client,
      k => return self.inspect_key_auto(k),
    })
  }
}

#[derive(Debug,Clone,Copy)]
pub struct PrintConfigOpt<'a>(pub &'a Option<String>);

impl PrintConfigOpt<'_> {
  #[throws(AE)]
  pub fn implement<'c, C: InspectableConfig + 'c>(
    self,
    configs: impl IntoIterator<Item=&'c C>,
  ) {
    if let Some(arg) = self.0 {
      for config in configs {
        Self::print_one_config(arg, config)?;
      }
      process::exit(0);
    }
  }

  pub fn keys(&self) -> impl Iterator<Item=&str> {
    self.0.as_ref().map(|arg| Self::split(arg)).into_iter().flatten()
  }

  fn split(arg: &str) -> impl Iterator<Item=&str> { arg.split(',') }

  #[throws(AE)]
  fn print_one_config(
    arg: &str,
    config: &dyn InspectableConfig,
  ) {
    let output = Self::split(arg)
      .map(|key| {
        if key == "pretty" {
          return Ok(format!("{:#?}", &config));
        }
        let insp = config.inspect_key(key)
          .ok_or_else(|| anyhow!("unknown config key {:?}", key))?;
        Ok::<_,AE>(DisplayInspectable(insp).to_string())
      })
      .collect::<Result<Vec<_>,_>>()?
      .join("\t");
    println!("{}",  output);
  }
}

pub trait InspectableConfigValue {
  fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result;
}
#[macro_export]
macro_rules! impl_inspectable_config_value {
  { $t:ty as $trait:path } => {
    impl InspectableConfigValue for $t {
      fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        <Self as $trait>::fmt(self, f)
      }
    }
  };
  
  { Vec<$t:ty> } => {
    impl InspectableConfigValue for Vec<$t> {
      #[throws(fmt::Error)]
      fn fmt(&self, f: &mut fmt::Formatter) {
        let mut first = Some(());
        for v in self.iter() {
          if first.take().is_none() { write!(f, " ")?; }
          InspectableConfigValue::fmt(v, f)?;
        }
      }
    }
  };
}

pub struct DisplayInspectable<'i>(pub &'i dyn InspectableConfigValue);
impl<'i> Display for DisplayInspectable<'i> {
  fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
    InspectableConfigValue::fmt(self.0, f)
  }
}

impl_inspectable_config_value!{ String as Display }
impl_inspectable_config_value!{ ServerName as Display }
impl_inspectable_config_value!{ ClientName as Display }
impl_inspectable_config_value!{ u16 as Display }
impl_inspectable_config_value!{ u32 as Display }
impl_inspectable_config_value!{ reqwest::Url as Display }

impl_inspectable_config_value!{ IpAddr as Display }
impl_inspectable_config_value!{ ipnet::IpNet as Display }
impl_inspectable_config_value!{ Vec<IpAddr> }
impl_inspectable_config_value!{ Vec<ipnet::IpNet> }

impl InspectableConfigValue for Duration {
  fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
    let v = self.as_secs_f64();
    Display::fmt(&v, f)
  }
}

#[ext(U32Ext)]
pub impl u32 {
  fn sat(self) -> usize { self.try_into().unwrap_or(usize::MAX) }
}

#[ext]
impl<'s> Option<&'s str> {
  #[throws(AE)]
  fn value(self) -> &'s str {
    self.ok_or_else(|| anyhow!("value needed"))?
  }
}

#[derive(Clone)]
pub struct Secret(pub String);
impl Parseable for Secret {
  #[throws(AE)]
  fn parse(s: Option<&str>) -> Self {
    let s = s.value()?;
    if s.is_empty() { throw!(anyhow!("secret value cannot be empty")) }
    Secret(s.into())
  }
  #[throws(AE)]
  fn default_for_ordinary() -> Self { Parseable::unspecified() }
  fn unspecified() -> Self { Secret(default()) }
}
impl Debug for Secret {
  #[throws(fmt::Error)]
  fn fmt(&self, f: &mut fmt::Formatter) { write!(f, "Secret(***)")? }
}
impl_inspectable_config_value!{ Secret as Debug }

#[derive(Debug,Clone,Hash,Eq,PartialEq)]
pub enum SectionName {
  Link(LinkName),
  Client(ClientName),
  Server(ServerName), // includes SERVER, which is slightly special
  ServerLimit(ServerName),
  GlobalLimit,
  Common,
}
pub use SectionName as SN;

#[derive(Debug)]
struct RawValRef<'v,'l,'s> {
  raw: Option<&'v str>, // todo: not Option any more
  key: &'static str,
  loc: &'l ini::Loc,
  section: &'s SectionName,
}

impl<'v> RawValRef<'v,'_,'_> {
  #[throws(AE)]
  fn try_map<F,T>(&self, f: F) -> T
  where F: FnOnce(Option<&'v str>) -> Result<T, AE> {
    f(self.raw)
      .with_context(|| format!(r#"file {:?}, section {}, key "{}""#,
                               self.loc, self.section, self.key))?
  }
}

pub struct Config {
  pub opts: CommonOpts,
}

static OUTSIDE_SECTION: &str = "[";
static SPECIAL_SERVER_SECTION: &str = "SERVER";

#[derive(Debug)]
struct Aggregate {
  end: LinkEnd,
  keys_allowed: HashMap<&'static str, SectionKindList>,
  sections: HashMap<SectionName, ini::Section>,
}

type OkAnyway<'f,A> = &'f dyn Fn(&io::Error) -> Option<A>;
#[ext]
impl<'f,A> OkAnyway<'f,A> {
  fn ok<T>(self, r: &Result<T, io::Error>) -> Option<A> {
    let e = r.as_ref().err()?;
    let a = self(e)?;
    Some(a)
  }
}

impl FromStr for SectionName {
  type Err = AE;
  #[throws(AE)]
  fn from_str(s: &str) -> Self {
    match s {
      "COMMON" => return SN::Common,
      "LIMIT" => return SN::GlobalLimit,
      _ => { }
    };
    if let Ok(n@ ServerName(_)) = s.parse() { return SN::Server(n) }
    if let Ok(n@ ClientName(_)) = s.parse() { return SN::Client(n) }
    let (server, client) = s.split_ascii_whitespace().collect_tuple()
      .ok_or_else(|| anyhow!(
        "bad section name {:?} \
         (must be COMMON, <server>, <client>, or <server> <client>",
        s
      ))?;
    let server = server.parse().context("server name in link section name")?;
    if client == "LIMIT" { return SN::ServerLimit(server) }
    let client = client.parse().context("client name in link section name")?;
    SN::Link(LinkName { server, client })
  }
}
impl Display for InstanceConfig {
  #[throws(fmt::Error)]
  fn fmt(&self, f: &mut fmt::Formatter) { Display::fmt(&self.link, f)? }
}

impl Display for SectionName {
  #[throws(fmt::Error)]
  fn fmt(&self, f: &mut fmt::Formatter) {
    match self {
      SN::Link  (ref l)      => Display::fmt(l, f)?,
      SN::Client(ref c)      => write!(f, "[{}]"       , c)?,
      SN::Server(ref s)      => write!(f, "[{}]"       , s)?,
      SN::ServerLimit(ref s) => write!(f, "[{} LIMIT] ", s)?,
      SN::GlobalLimit        => write!(f, "[LIMIT]"       )?,
      SN::Common             => write!(f, "[COMMON]"      )?,
    }
  }
}

impl Aggregate {
  fn new(
    end: LinkEnd,
    keys_allowed: HashMap<&'static str, SectionKindList>
  ) -> Self { Aggregate {
    end, keys_allowed,
    sections: default(),
  } }

  #[throws(AE)] // AE does not include path
  fn read_file<A>(&mut self, path: &Path, anyway: OkAnyway<A>) -> Option<A>
  {
    let f = fs::File::open(path);
    if let Some(anyway) = anyway.ok(&f) { return Some(anyway) }
    let mut f = f.context("open")?;

    let mut s = String::new();
    let y = f.read_to_string(&mut s);
    if let Some(anyway) = anyway.ok(&y) { return Some(anyway) }
    y.context("read")?;

    self.read_string(s, path)?;
    None
  }

  #[throws(AE)] // AE does not include path
  fn read_string(&mut self, s: String, path_for_loc: &Path) {
    let mut map: ini::Parsed = default();
    ini::read(&mut map, &mut s.as_bytes(), path_for_loc)
      .context("parse as INI")?;
    if map.get(OUTSIDE_SECTION).is_some() {
      throw!(anyhow!("INI file contains settings outside a section"));
    }

    for (sn, section) in map {
      let sn = sn.parse().dcontext(&sn)?;
      let vars = &section.values;

      for (key, val) in vars {
        (||{
          let skl = if key == "server" {
            SKL::ServerName
          } else {
            *self.keys_allowed.get(key.as_str()).ok_or_else(
              || anyhow!("unknown configuration key")
            )?
          };
          if ! skl.contains(&sn, self.end) {
            throw!(anyhow!("key not applicable in this kind of section"))
          }
          Ok::<_,AE>(())
        })()
          .with_context(|| format!("key {:?}", key))
          .with_context(|| val.loc.to_string())?
      }

      let ent = self.sections.entry(sn)
        .or_insert_with(|| ini::Section {
          loc: section.loc.clone(),
          values: default(),
        });

      for (key, ini::Val { val: raw, loc }) in vars {
        let val = if raw.starts_with('\'') || raw.starts_with('"') {
          (||{
            if raw.contains('\\') {
              throw!(
                anyhow!("quoted value contains backslash, not supported")
              );
            }
            let quote = &raw[0..1];

            let unq = raw[1..].strip_suffix(quote)
              .ok_or_else(
                || anyhow!("mismatched quotes around quoted value")
              )?
              .to_owned();
            if unq.contains(quote) {
              throw!(anyhow!(
                "quoted value contains quote (escaping not supported)"
              ))
            }

            Ok::<_,AE>(unq)
          })()
            .with_context(|| format!("key {:?}", key))
            .with_context(|| loc.to_string())?
        } else {
          raw.clone()
        };
        let key = key.replace('-',"_");
        ent.values.insert(key, ini::Val { val, loc: loc.clone() });
      }
    }
  }

  #[throws(AE)] // AE includes path
  fn read_dir_d<A>(&mut self, path: &Path, anyway: OkAnyway<A>) -> Option<A>
  {
    let dir = fs::read_dir(path);
    if let Some(anyway) = anyway.ok(&dir) { return Some(anyway) }
    let dir = dir.context("open directory").dcontext(path)?;
    for ent in dir {
      let ent = ent.context("read directory").dcontext(path)?;
      let leaf = ent.file_name();
      let leaf = leaf.to_str();
      let leaf = if let Some(leaf) = leaf { leaf } else { continue }; //utf8?
      if leaf.len() == 0 { continue }
      if ! leaf.chars().all(
        |c| c=='-' || c=='_' || c.is_ascii_alphanumeric()
      ) { continue }

      // OK we want this one
      let ent = ent.path();
      self.read_file(&ent, &|_| None::<Void>).dcontext(&ent)?;
    }
    None
  }

  #[throws(AE)] // AE includes everything
  fn read_toplevel(&mut self, toplevel: &Path) {
    enum Anyway { None, Dir }
    match self.read_file(toplevel, &|e| match e {
      e if e.kind() == EK::NotFound => Some(Anyway::None),
      e if e.is_is_a_directory() => Some(Anyway::Dir),
      _ => None,
    })
      .dcontext(toplevel).context("top-level config directory (or file)")?
    {
      None | Some(Anyway::None) => { },

      Some(Anyway::Dir) => {
        struct AnywayNone;
        let anyway_none = |e: &io::Error| match e {
          e if e.kind() == EK::NotFound => Some(AnywayNone),
          _ => None,
        };

        let mk = |leaf: &str| {
          [ toplevel, &PathBuf::from(leaf) ]
            .iter().collect::<PathBuf>()
        };

        for &(try_main, desc) in &[
          ("main.cfg", "main config file"),
          ("master.cfg", "obsolete-named main config file"),
        ] {
          let main = mk(try_main);

          match self.read_file(&main, &anyway_none)
            .dcontext(main).context(desc)?
          {
            None => break,
            Some(AnywayNone) => { },
          }
        }

        for &(try_dir, desc) in &[
          ("config.d", "per-link config directory"),
          ("secrets.d", "per-link secrets directory"),
        ] {
          let dir = mk(try_dir);
          match self.read_dir_d(&dir, &anyway_none).context(desc)? {
            None => { },
            Some(AnywayNone) => { },
          }
        }
      }
    }
  }

  #[throws(AE)] // AE includes extra, but does that this is extra
  fn read_extra(&mut self, extra: &Path) {
    struct AnywayDir;

    match self.read_file(extra, &|e| match e {
      e if e.is_is_a_directory() => Some(AnywayDir),
      _ => None,
    })
      .dcontext(extra)?
    {
      None => return,
      Some(AnywayDir) => {
        self.read_dir_d(extra, &|_| None::<Void>)?;
      }
    }

  }
}

impl Aggregate {
  fn instances(&self, only_server: Option<&ServerName>) -> BTreeSet<LinkName> {
    let mut links:              BTreeSet<LinkName> = default();

    let mut secrets_anyserver:  BTreeSet<&ClientName> = default();
    let mut secrets_anyclient:  BTreeSet<&ServerName> = default();
    let mut secret_global       = false;

    let mut putative_servers   = BTreeSet::new();
    let mut putative_clients   = BTreeSet::new();

    let mut note_server = |s| {
      if let Some(only) = only_server { if s != only { return false } }
      putative_servers.insert(s);
      true
    };
    let mut note_client = |c| {
      putative_clients.insert(c);
    };

    for (section, vars) in &self.sections {
      let has_secret = || vars.values.contains_key("secret");
      //dbg!(&section, has_secret());

      match section {
        SN::Link(l) => {
          if ! note_server(&l.server) { continue }
          note_client(&l.client);
          if has_secret() { links.insert(l.clone()); }
        },
        SN::Server(ref s) => {
          if ! note_server(s) { continue }
          if has_secret() { secrets_anyclient.insert(s); }
        },
        SN::Client(ref c) => {
          note_client(c);
          if has_secret() { secrets_anyserver.insert(c); }
        },
        SN::Common => {
          if has_secret() { secret_global = true; }
        },
        _ => { },
      }
    }

    //dbg!(&putative_servers, &putative_clients);
    //dbg!(&secrets_anyserver, &secrets_anyclient, &secret_global);

    // Add links which are justified by blanket secrets
    for (client, server) in iproduct!(
      putative_clients.into_iter().filter(
        |c| secret_global
         || secrets_anyserver.contains(c)
         || ! secrets_anyclient.is_empty()
      ),
      putative_servers.iter().cloned().filter(
        |s| secret_global
         || secrets_anyclient.contains(s)
         || ! secrets_anyserver.is_empty()
      )
    ) {
      links.insert(LinkName {
        client: client.clone(),
        server: server.clone(),
      });
    }

    links
  }
}

struct ResolveContext<'c> {
  agg: &'c Aggregate,
  link: &'c LinkName,
  end: LinkEnd,
  all_sections: Vec<SectionName>,
}

trait Parseable: Sized {
  fn parse(s: Option<&str>) -> Result<Self, AE>;

  /// Used for lookups with [`ResolveContest::ordinary`] etc.
  ///
  /// Fails, if this setting ought to have been specified.
  /// The caller will add a key name to the error.
  fn default_for_ordinary() -> Result<Self, AE> {
    Err(anyhow!("setting must be specified"))
  }

  /// Placeholder (infalliable)
  ///
  /// Used (sometimes) for lookups with
  /// [`ResolveContest::client`],
  /// [`server`](`ResolveContest::server`) and
  /// [`computed`](`ResolveContest::server`).
  ///
  /// Ie, when the value need not be specified because
  /// it may not be applicable, or could be computed.
  ///
  /// We could use `Default::default` but
  /// not all the types we want to use implement that.
  fn unspecified() -> Self;
}

impl Parseable for Duration {
  #[throws(AE)]
  fn parse(s: Option<&str>) -> Duration {
    // todo: would be nice to parse with humantime maybe
    Duration::from_secs( s.value()?.parse()? )
  }
  fn unspecified() -> Duration { Duration::ZERO }
}
macro_rules! parseable_from_str { ($t:ty, $def:expr) => {
  impl Parseable for $t {
    #[throws(AE)]
    fn parse(s: Option<&str>) -> $t { s.value()?.parse()? }
    #[throws(AE)]
    fn default_for_ordinary() -> Self { Parseable::unspecified() }
    fn unspecified() -> Self { $def }
  }
} }
parseable_from_str!{u16, default() }
parseable_from_str!{u32, default() }
parseable_from_str!{String, default() }
parseable_from_str!{IpNet, default() }
parseable_from_str!{IpAddr, Ipv4Addr::UNSPECIFIED.into() }

parseable_from_str!{
  Url,
  "hippotat-unspecified:".parse()
    .expect("failed to parse `hippotat-unspecified:` as a url")
}

impl<T:Parseable> Parseable for Vec<T> {
  #[throws(AE)]
  fn parse(s: Option<&str>) -> Vec<T> {
    s.value()?
      .split_ascii_whitespace()
      .map(|s| Parseable::parse(Some(s)))
      .collect::<Result<Vec<_>,_>>()?
  }
  #[throws(AE)]
  fn default_for_ordinary() -> Self { Parseable::unspecified() }
  fn unspecified() -> Self { default() }
}


#[derive(Debug,Copy,Clone,Eq,PartialEq)]
enum SectionKindList {
  PerClient,
  Limited,
  Limits,
  Global,
  ServerName,
  None,
}
use SectionKindList as SKL;

impl SectionName {
  fn special_server_section() -> Self { SN::Server(ServerName(
    SPECIAL_SERVER_SECTION.into()
  )) }
}

impl SectionKindList {
  fn contains(self, s: &SectionName, end: LinkEnd) -> bool {
    match (self, end) {
      (SKL::PerClient,_) |
      (SKL::Global, LinkEnd::Client) => matches!(s, SN::Link(_)
                                                  | SN::Client(_)
                                                  | SN::Server(_)
                                                  | SN::Common),

      (SKL::Limits,_)     => matches!(s, SN::ServerLimit(_)
                                       | SN::GlobalLimit),

      (SKL::Global, LinkEnd::Server) => matches!(s, SN::Common
                                                  | SN::Server(_)),

      (SKL::Limited,_)    => SKL::PerClient.contains(s, end)
                           | SKL::Limits   .contains(s, end),

      (SKL::ServerName,_) => matches!(s, SN::Common)
                           | matches!(s, SN::Server(ServerName(name))
                                         if name == SPECIAL_SERVER_SECTION),
      (SKL::None,_)       => false,
    }
  }
}

impl Aggregate {
  fn lookup_raw<'a,'s,S>(&'a self, key: &'static str, sections: S)
                       -> Option<RawValRef<'a,'a,'s>>
  where S: Iterator<Item=&'s SectionName>
  {
    for section in sections {
      if let Some(val) = self.sections
        .get(section)
        .and_then(|s: &ini::Section| s.values.get(key))
      {
        return Some(RawValRef {
          raw: Some(&val.val),
          loc: &val.loc,
          section, key,
        })
      }
    }
    None
  }

  #[throws(AE)]
  pub fn establish_server_name(&self) -> ServerName {
    let key = "server";
    let raw = match self.lookup_raw(
      key,
      [ &SectionName::Common, &SN::special_server_section() ].iter().cloned()
    ) {
      Some(raw) => raw.try_map(|os| os.value())?,
      None => SPECIAL_SERVER_SECTION,
    };
    ServerName(raw.into())
  }
}

impl<'c> ResolveContext<'c> {
  fn first_of_raw(&'c self, key: &'static str, sections: SectionKindList)
                  -> Option<RawValRef<'c,'c,'c>> {
    self.agg.lookup_raw(
      key,
      self.all_sections.iter()
        .filter(|s| sections.contains(s, self.end))
    )
  }

  #[throws(AE)]
  fn first_of<T>(&self, key: &'static str, sections: SectionKindList)
                 -> Option<T>
  where T: Parseable
  {
    match self.first_of_raw(key, sections) {
      None => None,
      Some(raw) => Some(raw.try_map(Parseable::parse)?),
    }
  }

  #[throws(AE)]
  pub fn ordinary<T>(&self, key: &'static str, skl: SKL) -> T
  where T: Parseable
  {
    match self.first_of(key, skl)? {
      Some(y) => y,
      None => Parseable::default_for_ordinary()
        .with_context(|| key.to_string())?,
    }
  }

  #[throws(AE)]
  pub fn limited<T>(&self, key: &'static str, skl: SKL) -> T
  where T: Parseable + Ord
  {
    assert_eq!(skl, SKL::Limited);
    let val = self.ordinary(key, SKL::PerClient)?;
    if let Some(limit) = self.first_of(key, SKL::Limits)? {
      min(val, limit)
    } else {
      val
    }
  }

  #[throws(AE)]
  pub fn client<T>(&self, key: &'static str, skl: SKL) -> T
  where T: Parseable {
    match self.end {
      LinkEnd::Client => self.ordinary(key, skl)?,
      LinkEnd::Server => Parseable::unspecified(),
    }
  }
  #[throws(AE)]
  pub fn server<T>(&self, key: &'static str, skl: SKL) -> T
  where T: Parseable {
    match self.end {
      LinkEnd::Server => self.ordinary(key, skl)?,
      LinkEnd::Client => Parseable::unspecified(),
    }
  }

  #[throws(AE)]
  pub fn computed<T>(&self, _key: &'static str, skl: SKL) -> T
  where T: Parseable
  {
    assert_eq!(skl, SKL::None);
    Parseable::unspecified()
  }

  #[throws(AE)]
  pub fn special_ipif(&self, key: &'static str, skl: SKL) -> String {
    assert_eq!(skl, SKL::PerClient); // we tolerate it in per-client sections
    match self.end {
      LinkEnd::Client => self.ordinary(key, SKL::PerClient)?,
      LinkEnd::Server => self.ordinary(key, SKL::Global)?,
    }
  }

  #[throws(AE)]
  pub fn special_link(&self, _key: &'static str, skl: SKL) -> LinkName {
    assert_eq!(skl, SKL::None);
    self.link.clone()
  }

  #[throws(AE)]
  pub fn special_max_up(&self, key: &'static str, skl: SKL) -> u32 {
    assert_eq!(skl, SKL::Limited);
    match self.end {
      LinkEnd::Client => self.ordinary(key, SKL::Limited)?,
      LinkEnd::Server => self.ordinary(key, SKL::Limits)?,
    }
  }
}

impl InstanceConfig {
  #[throws(AE)]
  fn complete(&mut self, end: LinkEnd) {
    let mut vhosts = self.vnetwork.iter()
      .map(|n| n.hosts()).flatten()
      .filter({ let vaddr = self.vaddr; move |v| v != &vaddr });

    if self.vaddr.is_unspecified() {
      self.vaddr = vhosts.next().ok_or_else(
        || anyhow!("vnetwork too small to generate vaddrr")
      )?;
    }
    if self.vrelay.is_unspecified() {
      self.vrelay = vhosts.next().ok_or_else(
        || anyhow!("vnetwork too small to generate vrelay")
      )?;
    }

    let check_batch = {
      let mtu = self.mtu;
      move |max_batch, key| {
        if max_batch/2 < mtu {
          throw!(anyhow!("max batch {:?} ({}) must be >= 2 x mtu ({}) \
                          (to allow for SLIP ESC-encoding)",
                         key, max_batch, mtu))
        }
        Ok::<_,AE>(())
      }
    };

    match end {
      LinkEnd::Client => {
        if self.url == Url::unspecified() {
          let addr = self.addrs.get(0).ok_or_else(
            || anyhow!("client needs addrs or url set")
          )?;
          self.url = format!(
            "http://{}{}/",
            match addr {
              IpAddr::V4(a) => format!("{}", a),
              IpAddr::V6(a) => format!("[{}]", a),
            },
            match self.port {
              80 => format!(""),
              p => format!(":{}", p),
            })
            .parse().unwrap()
        }

        self.effective_http_timeout = {
          let a = self.http_timeout;
          let b = self.http_timeout_grace;
          a.checked_add(b).ok_or_else(
            || anyhow!("calculate effective http timeout ({:?} + {:?})", a, b)
          )?
        };

        {
          let t = self.target_requests_outstanding;
          let m = self.max_requests_outstanding;
          if t > m { throw!(anyhow!(
            "target_requests_outstanding ({}) > max_requests_outstanding ({})",
            t, m
          )) }
        }

        check_batch(self.max_batch_up, "max_batch_up")?;
      },

      LinkEnd::Server => {
        if self.addrs.is_empty() {
          throw!(anyhow!("missing 'addrs' setting"))
        }
        check_batch(self.max_batch_down, "max_batch_down")?;
      },
    }

    #[throws(AE)]
    fn subst(var: &mut String,
             kv: &mut dyn Iterator<Item=(&'static str, &dyn Display)>
    ) {
      let substs = kv
        .map(|(k,v)| (k.to_string(), v.to_string()))
        .collect::<HashMap<String, String>>();
      let bad = parking_lot::Mutex::new(vec![]);
      *var = regex_replace_all!(
        r#"%(?:%|\((\w+)\)s|\{(\w+)\}|.)"#,
        var,
        |whole, k1, k2| (|| Ok::<_,String>({
          if whole == "%%" { "%" }
          else if let Some(&k) = [k1,k2].iter().find(|&&s| s != "") {
            substs.get(k).ok_or_else(
              || format!("unknown key %({})s", k)
            )?
          } else {
            throw!(format!("bad percent escape {:?}", &whole));
          }
        }))().unwrap_or_else(|e| { bad.lock().push(e); "" })
      ).into_owned();
      let bad = bad.into_inner();
      if ! bad.is_empty() {
        throw!(anyhow!("substitution failed: {}", bad.iter().format("; ")));
      }
    }

    {
      use LinkEnd::*;
      type DD<'d> = &'d dyn Display;
      fn dv<T:Display>(v: &[T]) -> String {
        format!("{}", v.iter().format(" "))
      }
      let mut ipif = mem::take(&mut self.ipif); // lets us borrow all of self
      let s = &self; // just for abbreviation, below
      let vnetwork = dv(&s.vnetwork);
      let vroutes  = dv(&s.vroutes);

      let keys = &["local",       "peer",    "rnets",   "ifname"];
      let values = match end {
 Server => [&s.vaddr as DD      , &s.vrelay, &vnetwork, &s.ifname_server],
 Client => [&s.link.client as DD, &s.vaddr,  &vroutes,  &s.ifname_client],
      };
      let always = [
        ( "mtu",     &s.mtu as DD ),
      ];

      subst(
        &mut ipif,
        &mut keys.iter().cloned()
          .zip_eq(values)
          .chain(always.iter().cloned()),
      ).context("ipif")?;
      self.ipif = ipif;
    }
  }
}

trait ResolveGlobal<'i> where Self: 'i {
  fn resolve<I>(it: I) -> Self
  where I: Iterator<Item=&'i Self>;
}
impl<'i,T> ResolveGlobal<'i> for T where T: Eq + Clone + Debug + 'i {
  fn resolve<I>(mut it: I) -> Self
  where I: Iterator<Item=&'i Self>
  {
    let first = it.next().expect("empty instances no global!");
    for x in it { assert_eq!(x, first); }
    first.clone()
  }
}

#[throws(AE)]
pub fn read(opts: &CommonOpts, end: LinkEnd)
            -> (Option<ServerName>, Vec<InstanceConfig>)
{
  let agg = (||{
    let mut agg = Aggregate::new(
      end,
      InstanceConfig::FIELDS.iter().cloned().collect(),
    );

    agg.read_string(DEFAULT_CONFIG.into(),
                    "<build-in defaults>".as_ref())
      .expect("builtin configuration is broken");

    agg.read_toplevel(&opts.config)?;
    for extra in &opts.extra_config {
      agg.read_extra(extra).context("extra config")?;
    }

    //eprintln!("GOT {:#?}", agg);

    Ok::<_,AE>(agg)
  })().context("read configuration")?;

  let server_name = match end {
    LinkEnd::Server => Some(agg.establish_server_name()?),
    LinkEnd::Client => None,
  };

  let instances = agg.instances(server_name.as_ref());
  let mut ics = vec![];
  //dbg!(&instances);

  for link in instances {
    let rctx = ResolveContext {
      agg: &agg,
      link: &link,
      end,
      all_sections: vec![
        SN::Link(link.clone()),
        SN::Client(link.client.clone()),
        SN::Server(link.server.clone()),
        SN::Common,
        SN::ServerLimit(link.server.clone()),
        SN::GlobalLimit,
      ],
    };

    if rctx.first_of_raw("secret", SKL::PerClient).is_none() { continue }

    let mut ic = InstanceConfig::resolve_instance(&rctx)
      .with_context(|| format!("resolve config for {}", &link))?;

    ic.complete(end)
      .with_context(|| format!("complete config for {}", &link))?;

    ics.push(ic);
  }

  (server_name, ics)
}

pub async fn startup<F,T,G,GFut,U>(progname: &str, end: LinkEnd,
                    opts: &CommonOpts, logopts: &LogOpts,
                    f: F, g: G) -> U
where F: FnOnce(Option<ServerName>, &[InstanceConfig]) -> Result<T,AE>,
      G: FnOnce(T, Vec<InstanceConfig>) -> GFut,
      GFut: Future<Output=Result<U,AE>>,
{
  async {
    dedup_eyre_setup()?;
    let (server_name, ics) = config::read(opts, end)?;

    let t = f(server_name, &ics)?;
    if ics.is_empty() { throw!(anyhow!("no associations, quitting")); }

    logopts.log_init()?;
    let u = g(t, ics).await?;

    Ok::<_,AE>(u)
  }.await.unwrap_or_else(|e| {
    eprintln!("{}: startup error: {}", progname, &e);
    process::exit(8);
  })
}
