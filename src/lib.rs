// Copyright 2021-2022 Ian Jackson and contributors to Hippotat
// SPDX-License-Identifier: GPL-3.0-or-later WITH LicenseRef-Hippotat-OpenSSL-Exception
// There is NO WARRANTY.

//! Library crate for Hippotat
//!
//! These libraries are for use within the Hippotat cargo package.
//! They do not have a stable API.
//!
//! Please refer to the
//! [project documentation](https://www.chiark.greenend.org.uk/~ianmdlvl/hippotat/current/docs/)

#![allow(clippy::style)]

#![allow(clippy::clone_on_copy)]
#![allow(clippy::map_flatten)]
#![allow(clippy::match_single_binding)]
#![allow(clippy::single_char_pattern)]
#![allow(clippy::unit_arg)]
#![allow(clippy::useless_format)]

pub mod prelude;

pub mod compat;
pub mod config;
pub mod ipif;
pub mod multipart;
pub mod slip;
pub mod reporter;
pub mod queue;
pub mod types;
pub mod utils;

pub mod ini;
