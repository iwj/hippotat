Configuration settings
======================

Exceptional settings
--------------------

* `server`

  Specifies `<servername>`.
  Is looked up in `[SERVER]` and `[COMMON]` only.
  If not specified there, it is `SERVER`.

  Used by server to select the appropriate parts of the
  rest of the configuration.  Ignored by the client.

* `secret`

  Looked up in the usual way, but used by client and server to
  determine which possible peerings to try to set up, and which to
  ignore.

  We define the sets of putative clients and servers, as follows:
  all those, for which there is any section (even an empty one)
  whose name is based on `<client>` or `<servername>` (as applicable).
  (`LIMIT` sections do not count.)

  The server queue packets for, and accept requests from, each
  putative client for which the config search yields a secret.

  Each client will create a local interface, and try to communicate
  with the server, for each possible pair (putative server,
  putative client) for which the config search yields a secret.

  The value is a string, fed directly into HMAC.

* `ipif`

  Command to run to create and communicate with local network
  interface.  Passed to sh -c.  Must speak SLIP on stdin/stdout.

  (For compatibility with older hippotat, `%(var)s` is supported too
  but this is deprecated since the extra `s` is confusing.)

  On server: applies to all clients; not looked up in client-specific sections.
  On client: may be different for different servers.

  [string; `userv root ipif %{local},%{peer},%{mtu},slip '%{rnets}'`]

### `ipif` interpolations:

The following interpolations aare substituted in the value for `ipif`:

| Input          | `%{local}`  | `%{peer}`  | `%{rnets}`  | `%{ifname}`    |
| -------------- | ----------- | ---------- | ----------- | -------------- |
| **on server**  | `vaddr`     | `vrelay`   | `vnetwork`  | `ifname_server` |
| **on client**  | `client`    | `vaddr`    | `vroutes`   | `ifname_client` |

**Always:** `%{mtu}`, and `%%` to indicate a literal `%`.


Capped settings
---------------

Values in `[<server> LIMIT]` and `[LIMIT]` are a cap (maximum) on
those from the other sections (including `COMMON`).  If a larger
value is obtained, it is (silently) reduced to the limit value.


* `max_batch_down`

  Size limit for response payloads.

  On client, incoming response bodies are limited to this (plus
  a fixed constant metadata overhead).
  Server uses minimum of client's and server's configured values
  (old servers just use server's value).

  [`65536` (bytes); `LIMIT`: `262144`]

* `max_batch_up`

  Size limit for request upbound payloads.  On client, used directly,
  with `LIMIT` applied.

  On server, only `LIMIT` is relevant, and must be at least the
  client's configured value (checked).

  [`4000` (bytes); `LIMIT`: `262144`]

* `max_queue_time`

  Discard packets after they have been queued this long
  waiting for http.

  On server: setting applies to downward packets.
  On client: setting applies to upward packets.

  [`10` (s); `LIMIT`: `121`]

* `http_timeout`

  On server: return with empty payload any http request oustanding
  for this long.

  On client: give up on any http request outstanding for
  for this long plus `http_timeout_grace`.

  Warning messages about link problems, printed by the client,
  are rate limited to no more than one per effective timeout.

  Client's effective timeout must be at least server's (checked).

  [`30` (s); `LIMIT`: `121`]

* `target_requests_outstanding`

  On client: try to keep this many requests outstanding, to
  allow for downbound data transfer.
  On server: whenever number of outstanding requests for
  a client exceeds this, returns oldest with empty payload.
  Must match between client and server (checked).
  [`3`; `LIMIT`: `10`]


Ordinary settings, used by both, not client-specific
----------------------------------------------------

On the server these are forbidden in the client-specific config
sections.

* `addrs`

  Public IP (v4 or v6) address(es) of the server; space-separated.
  On server: mandatory; used for bind.
  On client: used only to construct default `url`.
  No default.

* `vnetwork`

  Private network range.  Must contain all
  `<client>`s.  Must contain `vaddr` and `vrelay`, and is used
  to compute their defaults.  [CIDR syntax (`<prefix>/<length>`);
  `172.24.230.192/28`]

* `vaddr`

  Address of server's virtual interface.
  [default: first host entry in `vnetwork`, so `172.24.230.193`]

* `vrelay`

  Virtual point-to-point address used for tunnel routing
  (does not appear in packets).
  [default: first host entry in `vnetwork` other than `vaddr`,
  so `172.24.230.194`]

* `port`

  Public port number of the server.
  On server: used for bind.
  On client: used only to construct default url.
  [`80`]

  Do not set this to `443` -
  the server will speak plain unencrypted HTTP on the port you specify,
  which would be wrong for `443`.
  While the client has integrated TLS support, the server does not.
  To use hippotat with TLS:
   - Set up a TLS reverse proxy (such as apache or nginx),
     probably with a certificate from Let's Encrypt.
   - Configure `port` and `addrs` to the internal address and port
     (to which the reverse proxy forwards the requests).
   - Configure `url` to the public URL of the reverse proxy.

* `mtu`

  Of virtual interface.
  Must match exactly at each end (checked).
  [`1500` (bytes)]

  
Ordinary settings, used by server only
--------------------------------------

* `max_clock_skew`

  Permissible clock skew between client and server.
  Hippotat will not work if clock skew is more than this.
  Conversely: when moving client from one public network to
  another, the first network can deny service to the client for
  this period after the client leaves the first network.
  [`300` (s)]

* `ifname_server`

  Virtual interface name on the server.  [`shippo%d`]
  NB: any `%d` is interpolated (by the kernel).


Ordinary settings, used by client only
--------------------------------------

* `http_timeout_grace`

  See `http_timeout`.  [`5` (s)]

* `max_requests_outstanding`

  Client will hold off sending more requests than this to
  server even if it has data to send.  [`6`]

* `success_report_interval`

  If nonzero, report success periodically.  Otherwise just
  report it when we first have success.  [`3600` (s)]

* `http_retry`

  If a request fails, wait this long before considering it
  "finished" - to limit rate of futile requests (and also
  to limit rate of moaning on stderr).  [`5` s]

* `url`

  Public url of server.
  [`http://<first-entry-in-addrs>:<port>/`]

* `vroutes`

  Additional virtual addresses to be found at the server
  end, space-separated.  Routes to those will be created on
  the client.  `vrelay` is included implicitly.
  [CIDR syntax, space separated; default: none]

* `ifname_client`

  Virtual interface name on the client.  [`hippo%d`]
  NB: any `%d` is interpolated (by the kernel).
