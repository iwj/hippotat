Colophon and references
=======================

Hippotat is Copyright 2017-2022 Ian Jackson
and contributors.

Hippotat is released under the GNU GPLv3+
with an OpenSSL linking exception.
See the file `COPYING` in the source tree
for the full licence text.
There is NO WARRANTY.

The
[Documentation for the current version](https://www.chiark.greenend.org.uk/~ianmdlvl/hippotat/current/docs/)
is online,
as well as [for earlier versons](https://www.chiark.greenend.org.uk/~ianmdlvl/hippotat/).

The release notes are maintained in the
[Debian-for amt changelog](https://salsa.debian.org/iwj/hippotat/-/blob/main/debian/changelog)

[Hippotat's source repository](https://salsa.debian.org/iwj/hippotat)
is hosted on Debian Salsa.
