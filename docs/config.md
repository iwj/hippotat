Configuration scheme
====================

Configuration is in an INI-like format.  Sections start with lines
`[...]`.  Every setting must be in a section.  `#` and `;`
comment lines are supported.  Settings are `nmae = value`.  Whitespace
around the name and value is ignored.

The configuration files are resolved to a configuration for each
pairwise **link** between a client and a server.

Sections
--------

The same config key may appear in multiple sections; for example, in a
section specific to a link, as well as one for all links to a server.

Unless otherwise specified, any particular config setting for a
particular link is the value from the first of the following
applicable sections, or failing that the built-in default:

  * `[<servername> <client>]`
  * `[<client>]`
  * `[<servername>]` (often `[SERVER]`)
  * `[COMMON]`

`<client>` is the client's virtual address in IPv4 or IPv6 literal
syntax (without any surrounding `[..]`.

`<servername>` must be in the syntrax of a valid lowercase DNS
hostname (and not look like an address), or be literally `SERVER`.

There are also these special sections:

 * `[<servername> LIMIT]`
 * `[LIMIT]`

Files
-----

Both client and server read the files

 * `/etc/hippotat/main.cfg` (if it exists)
 * `/etc/hippotat/config.d/*`
 * `/etc/hippotat/secrets.d/*`

Here `*` means all contained files whose names consists of only
ascii alphanumerics plus `-` and `_`.

The `--config` option can be used to override the directory (usually
`/etc/hippotat`).  Additonally each `--extra-config` option names
an existing file or directory to be processed analagously.

The ini file format sections from these files are all unioned.  Later
files (in the list above, or alphabetically later) can override
settings from earlier ones.

Note that although it is conventional for information for a particular
server or client to be in a file named after that endpoint, there is
no semantic link: all the files are always read and the appropriate
section from each is applied to every link.

(If `main.cfg` does not exist, `master.cfg` will be tried for
backward compatibility reasons.)
