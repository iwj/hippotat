Installation
============

Hippotat is written in Rust.
The documentation is procssed with Sphinx.

It is most convenient to install (especailly the server) as a `.deb`.

On a recent-enough Debian derivative you can simply run
(on each end as applicable):
```
    apt install hippotat-server
    apt install hippotat-client
```

Building
--------

Obtain the source code from the canonical server:
<https://salsa.debian.org/iwj/hippotat/>

### Building with vanilla Debian tooling

On a Debian (or derivative) with compatible dependencies,
you can use the standard Debian build runes:

```
   apt-get build-dep .
   dpkg-buildpackage -uc -b
```

At the time of writing this works on Debian testing (aka "bookworm").

### Building binary packages with hybrid tooling

Because of impedance mismatches between
distros' approaches to packaging,
and the language specifkc package manager treadmill,
it can easily happy that you can't build on your Debian release.
You can use a hybrid approach instead:

On a system with a new enough `cargo` and `rustc`:

```
   apt-get -Pcargo-upstream build-dep .
   CARGO='cargo --locked' dpkg-buildpackage -Pcargo-upstream -uc -b
```

Sadly, Debian bullseye is not new enough;
to build there you will need to install Rust from upstream e.g.
[using Rustup](https://www.rust-lang.org/learn/get-started).

In any case, the build runes above will download dependencies
from the upstream Rust package repository.
The precise versions of those dependencies, and their precise contents,
are controlled and checked
by the `Cargo.lock` file shipped in the Hippotat source tree,
provided that `cargo --locked` is used.
So the invocation above is *not* equivalent to some `curl|bash` rune.

### Building executables with upstream Rust ecosystem tooling

If you don't want to install debs,
you can also just use cargo to build the two binaries,
`hippotat` and `hippotatd`:
```
    cargo build --locked --release
```

Hippotat releases are also published to `crates.io`, so
you could perhaps even `cargo install hippotat`.
Like any `cargo install`,
this *is* morally equivalent to `curl|bash`.


Installation
------------

 * On the server: `apt install hippotat-{server,common}_*.deb`
 * On the client: `apt install hippotat-{client,common}_*.deb`

This will also install the `userv` privsep tool,
and the `userv-ipif` utility for unprivileged network interface management.


Configuration
-------------

You will need to:

 1. Choose a suitable URL that Hippotat will be able to listen on.
    You can use a reverse proxy
    (but you may want to suppress some of the logging).
    The URL should be port 80, or 443 with TLS (with a reverse proxy).
    If using a reverse proxy,
    you must choose an internal IP port for the Hippotat server to use.

 2. Select a private network range for use by the IP-over-HTTP system,
    and assign addresses to the server and to each client.
    If you use a range from RFC1918, choose it at random,
    eg using the [Cambridge G-RIN](https://www.ucam.org/cam-grin/).

 3. Configure your firewalls to restrict access from that range
    to internal resources
    (eg, which might otherwise trust Hippotat addresses due to them
    being in RFC1918 private use ranges).
    Note that
    **Hippotat does not provide strong authentication or confidentiality**.

 4. Configure `hippotat` itself, at both ends.

 5. Configure `authbind` to allow the service user `_hippotat`
    (created by the package installation)
    to bind to the port you have chosen,
    on the server.

 6. Configure `ipif` to allow
    the service user `_hippotat` (on the server)
    or your own user (on the client)
    to create network interfaces
    using addresses in the range you have assigned.

The last two steps can be done automatically with the
`hippotat-setup-permissions` script:
run `hippotat-setup-permissions server` on the server,
and `hippotat-setup-permissions client` on the client,
after having configured hippotat itself.
For more information on what this script does,
consult its manpage.

Startup - server
----------------

The `hippotat-server` package
supplies an init script
which will start the `hippotatd` server program,
if `/etc/hippotat/main.cfg` exists.
If you just created that file, 
`service hippotatd start` will start the server..

Consult the init script to see options you can put in
`/etc/default/hippotat`.


Usage - client
--------------

It is not usual to have hippotat running all the time,
since its approach is rather wasteful,
and not needed in a sensible network environment.

When you find yourself in a bad network environment,
run `hippotat` from a shell.
It will bring up the hippotat link.
When you don't need Hippotat any more, simply `^C` it.

With the link up you can `ssh` (or, maybe, `mosh`) to the server,
using the server's Hippotat IP address.
If you are using [secnet](https://www.chiark.greenend.org.uk/ucgi/~ianmdlvl/git/secnet.git/)'s polypath feature,
secnet will automatically start to use hippotat.

Don't send traffic you care about unencrypted over Hippotat.
Hippotat's security mechanisms are designed
to try to minimise denial of service,
especially by off-path entities,
but **data confidentiality and integrity are not provided**.


Troubleshooting
---------------

The server will log to syslog, facility `daemon`, by default.

The client will report its progress to stderr.
