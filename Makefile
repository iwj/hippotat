# Copyright 2020-2022 Ian Jackson and contributors to Hippotat
# SPDX-License-Identifier: GPL-3.0-or-later WITH LicenseRef-Hippotat-OpenSSL-Exception
# There is NO WARRANTY.

SHELL=/bin/bash

default: all

SPHINXBUILD	?= sphinx-build

INSTALL		?= install

ifneq (,$(NAILING_CARGO))

NAILING_CARGO ?= nailing-cargo
CARGO = $(NAILING_CARGO)
BUILD_SUBDIR ?= ../Build
TARGET_DIR ?= $(BUILD_SUBDIR)/$(notdir $(PWD))/target
NAILING_CARGO_JUST_RUN ?= $(NAILING_CARGO) --no-nail --no-cargo-lock-manip -q ---

else

CARGO		?= cargo
TARGET_DIR	?= target

endif # $(NAILING_CARGO)

CARGO_RELEASE ?= release
TARGET_RELEASE_DIR ?= $(TARGET_DIR)/$(CARGO_RELEASE)

ifneq (debug,$(CARGO_RELEASE))
CARGO_RELEASE_ARG ?= --$(CARGO_RELEASE)
endif

rsrcs = $(shell $(foreach x,$(MAKEFILE_FIND_X),set -$x;)\
    find -H $1 \( -name Cargo.toml -o -name Cargo.lock -o -name Cargo.lock.example -o -name \*.rs \) )
stamp=@mkdir -p stamp; touch $@

TESTS=$(notdir $(wildcard test/t-*[^~]))
MAN8PAGES=hippotat-setup-permissions.8
MANPAGES=$(MAN8PAGES)
man8dir=/usr/share/man/man8

all:	cargo-build doc

check:	cargo-test $(addprefix stamp/,$(TESTS))

cargo-build: stamp/cargo-build
cargo-test: stamp/cargo-test

stamp/cargo-%: $(call rsrcs,.)
	+$(CARGO) $* $(CARGO_RELEASE_ARG) $(CARGO_BUILD_OPTIONS) --workspace
	$(stamp)

stamp/t-%: test/t-% stamp/cargo-build $(wildcard test/*[^~])
	TARGET_RELEASE_DIR=$(abspath $(TARGET_RELEASE_DIR)) \
	$(NAILING_CARGO_JUST_RUN) \
	$(abspath test/capture-log) tmp/t-$*.log \
	$(abspath test/go-with-unshare test/t-$*)
	@echo OK t-$*; touch $@

doc:	docs/html/index.html $(MANPAGES)
	@echo 'Documentation can now be found here:'
	@echo '  file://$(PWD)/$<'

docs/html/index.html: docs/conf.py $(wildcard docs/*.md docs/*.rst docs/*.png)
	rm -rf docs/html
	$(SPHINXBUILD) -M html docs docs $(SPHINXOPTS)

hippotat-setup-permissions%: hippotat-setup-permissions%.pod
	m=$@; pod2man --section=$${m##*.} --date="Hippotat" \
		--center=" " --name=$${m%.*} \
		$^ $@

doch=/usr/share/doc/hippotat/

install: all
	$(INSTALL) -d $(DESTDIR)/usr/{bin,sbin}
	$(INSTALL) -d $(DESTDIR)$(doch) $(DESTDIR)$(man8dir)
	$(INSTALL) -m 755 $(TARGET_RELEASE_DIR)/hippotat $(DESTDIR)/usr/bin/.
	$(INSTALL) -m 755 $(TARGET_RELEASE_DIR)/hippotatd $(DESTDIR)/usr/sbin/.
	$(INSTALL) -m 755 hippotat-setup-permissions $(DESTDIR)/usr/bin/.
	cp -r docs/html $(DESTDIR)$(doch)
	$(INSTALL) -m 644 PROTOCOL.txt $(DESTDIR)$(doch)/
	$(INSTALL) -m 644 $(MAN8PAGES) $(DESTDIR)$(man8dir)/.

clean:
	rm -rf stamp/* tmp
	rm -rf docs/html docs/doctrees
	rm -f hippotat-setup-permissions.8

very-clean: clean
	+$(CARGO) clean

#---------- release process ----------
#
# Preparatory
#   cargo update and/or upgrades
#
# Checks
#   nailing-cargo -o audit
#   git clean -xdff && NAILING_CARGO=nailing-cargo make check
#   dgit -wgfa sbuild -c build -A
#   autopkgtest --ignore-restrictions=isolation-machine . --- schroot build
#
# Update release notes in debian/changelog:
#
# Update versions
#   * Cargo.toml		version
#   * macros/Cargo.toml		version
#   * debian/changelog		finalise
#
# Squash branch if need be.
#
#   VERSION=$(dpkg-parsechangelog -SVersion); echo $VERSION
#
# Rerun checks
#   * See "Check" above
#   * release-rust-crate --dry-run hippotat $VERSION
#
# == commitment point ==
#
#   make version update MR (see above), merge into main
#
#   release-rust-crate hippotat $VERSION
#   make publish publish-make-current PUBLISH_VERSION=$VERSION
#   dgit -wgfa push-source sid
#   git-push --dry-run --follow-tags origin
#   git-push --follow-tags origin
#
#   release announcement on mailing list and/or blog post

#---------- docs publication ----------

PUBLISHED_BRANCH=published
PUBLISH_VERSION=unreleased
PUBLISH_USER=ianmdlvl@login.chiark.greenend.org.uk
PUBLISH_DOC_SPHINX_BASE=public-html/hippotat
PUBLISH_DOC_SPHINX_TAIL=$(PUBLISH_VERSION)/docs
PUBLISH_DOC_SPHINX=$(PUBLISH_USER):$(PUBLISH_DOC_SPHINX_BASE)/$(PUBLISH_DOC_SPHINX_TAIL)

publish: doc
	ssh $(PUBLISH_USER) 'cd $(PUBLISH_DOC_SPHINX_BASE) && mkdir -p $(PUBLISH_DOC_SPHINX_TAIL)'
	rsync -r --delete-delay docs/html/. $(PUBLISH_DOC_SPHINX)/.
	git branch -f $(PUBLISHED_BRANCH)

publish-make-current:
	ssh $(PUBLISH_USER) 'set -e; cd $(PUBLISH_DOC_SPHINX_BASE); rm -f current.tmp; ln -s $(PUBLISH_VERSION) current.tmp; mv -T current.tmp current'

.PHONY: cargo-build all doc clean
