# Hacking on hippotat

(This file is not very comprehensive.)

## Testing

Using upstream dependencies

```
NAILING_CARGO=nailing-cargo make check
```

Using dependencies from Debian

```
autopkgtest --ignore-restrictions=isolation-machine . --- schroot build
```

Run one test, ad-hoc, using a debug build

```
nailing-cargo build
nailing-cargo --- test/with-unshare test/t-basic
```
