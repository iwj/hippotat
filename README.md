Hippotat - asinine IP over HTTP
===============================

Hippotat is a system to allow you to use your normal VPN,
ssh, and other applications, even in broken network environments
that are only ever tested with "web stuff".

Packets are parcelled up into HTTP POST requests, resembling
form submissions (or JavaScript XMLHttpRequest traffic),
and the returned packets arrive via the HTTP response bodies.

Scenario
--------

You're in a cafe or a hotel, trying to use the provided wifi.
But it's not working.  You discover that port 80 and port 443
are open, but the wifi forbids all other traffic.

Never mind, start up your hippotat client.  Now you have connectivity.
Your VPN and SSH and so on run over Hippotat.
The result is not very efficient, but it does work.

The design goal is that if the your barista's phone works OK,
or the hotel concierge can see Google on their computer,
you can use the internet properly, despite
whatever breakage and nonsense.

So Hippotat is an alternative to the futile strategy of
trying to report technical bugs, or stupid portblocks,
in terrible wifi systems.

Of course it can't always help.
If the wifi is bad enough that one's hosts'
devices don't work reliably either,
hopefully you can probably get them to reboot the magic box,
or maybe get some money off, if wifi was supposed to be included.

Non-goals
---------

**Hippotat does not provide meaningful encryption**.
You should use protocols over the top of it
that you would be happy to run over the public internet:
encrypted ones, like a VPN or SSH.

Use of Hippotat is not intended to be undetectable,
or even particularly hard to distinguish from other uses of HTTP,
should someone want to go to the effort.
Rather, it is intended to be deployed against idiocy, ignorance,
and incompetence.

Protection against interference is limited to
trying to defend against off-path attackers, and 
arranging that formerly-on-path attackers'
ability to do harm will expire reasonably soon.

Hippotat is not designed to allow you to "leech" internet access
from "closed" Wifi.
It won't work if "normal web access" doesn't.
You might try IP-over-DNS systems for that.
